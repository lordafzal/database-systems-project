#!/usr/bin/env python

import argparse
import common.config
import common.args
from .view import price_to_string, heartbeat_to_string


def main():
    """
    Stream the prices for a list of Instruments for the active Account.
    """

    parser = argparse.ArgumentParser()

    common.config.add_argument(parser)
    
    parser.add_argument(
        '--instrument', "-i",
        type=common.args.instrument,
        required=True,
        action="append",
        help="Instrument to get prices for"
    )

    parser.add_argument(
        '--snapshot',
        action="store_true",
        default=True,
        help="Request an initial snapshot"
    )

    parser.add_argument(
        '--no-snapshot',
        dest="snapshot",
        action="store_false",
        help="Do not request an initial snapshot"
    )

    parser.add_argument(
        '--show-heartbeats', "-s",
        action='store_true',
        default=False,
        help="display heartbeats"
    )

    args = parser.parse_args()

    account_id = args.config.active_account
    
    api = args.config.create_streaming_context()

    # api.set_convert_decimal_number_to_native(False)

    # api.set_stream_timeout(3)

    #
    # Subscribe to the pricing stream
    #
    response = api.pricing.stream(
        account_id,
        snapshot=args.snapshot,
        instruments=",".join(args.instrument),
    )
    instrument = ",".join(args.instrument)
    #
    # Print out each price as it is received
    #
    count = 0    
    for msg_type, msg in response.parts():
        ##while count < 5:
        if msg_type == "pricing.Heartbeat" and args.show_heartbeats:
            print(heartbeat_to_string(msg))
        elif msg_type == "pricing.Price":
            print(price_to_string(msg))
            if count < 5:
                string = price_to_string(msg)
                string = string.split()
                str2 = string[-1].split("/")
                if count == 0:
                    temp1 = float(str2[0])
                    temp2 = float(str2[1])
                if count == 4:
                    slope1 = (float)(float(str2[0])-temp1)/((count+1) - 0)
                    slope2 = (float)(float(str2[1])-temp2)/((count+1) - 0)
                    if slope1 <= 0:
                        ans1 = "Down"
                    elif slope1 > 0:
                        ans1 = "Up"
                    if slope2 <= 0:
                        ans2 = "Down"
                    elif slope2 > 0:
                        ans2 = "Up"                    
                    print(ans1+"/"+ans2)
                    print(slope1 - slope2)
                    if (slope1 - slope2) <= 0:
                        sig = 'down'
                    if (slope1 - slope2) > 0:
                        sig = "up"
                    
                    print(instrument)
                    
                    with open(instrument+".txt", "w") as text_file:
                        text_file.write(instrument+":"+sig)
                    
                ##print(str2)
                count += 1
            if count == 5:
                count = 0
if __name__ == "__main__":
    main()
