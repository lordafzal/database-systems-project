import csv, time, os

def parseFile(filename):
    # Epoch stuff, need correct timezone for consistency
    os.environ['TZ']='UTC'
    rawData = []

    # Parsing through the CSV file
    with open(filename) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:

            # Turning 20010102230100 of format YYYYmmddHHMMSS to
            #   YYYY-mm-dd-HH-MM-SS for strptime to work
            timeCat = str(row[1]) + str(row[2])
            timeCat = list(timeCat)
            timeCat.insert(4,'-')
            timeCat.insert(7,'-')
            timeCat.insert(10,'-')
            timeCat.insert(13,'-')
            timeCat.insert(16,'-')
            timeCat = ''.join(timeCat)
            pattern = '%Y-%m-%d-%H-%M-%S'

            # Python shit for turning time string to a epoch
            epoch = int(time.mktime(time.strptime(timeCat,pattern)))
            line = []
            line.append(epoch)

            # Appending avergae price for that minute
            line.append((float(row[3])+float(row[6]))/2)
            rawData.append(line)
    return rawData


dataArray = parseFile('EURUSDTST.csv')
print(dataArray)
