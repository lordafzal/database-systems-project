#!/bin/bash
echo "Building Large Index Table"
psql "Prices" -c "DROP TABLE temp;"
tickers=("GSPC" "DJI" "IXIC" "NYA" "XAX" "BUK100P" "RUT" "VIX" "GDAXI" "FCHI" "STOXX50E" "N100" "BFX" "HSI" "SS" "AXJO" "AORD" "BSESN" "JKSE" "NZ50" "KS11" "TWII" "GSPTSE" "BVSP" "MXX" "IPSA" "MERV" "TA125TA" "JN0UJO")
counter=0
while [ $counter -le $(expr ${#tickers[@]} - 1) ]
do
echo 'Scrubbing duplicates from: ' ${tickers[$counter]}
psql "Prices" -c "SELECT * INTO temp FROM (SELECT DISTINCT * FROM Prices."${tickers[$counter]}") AS dist;"
psql "Prices" -c "DROP TABLE Prices."${tickers[$counter]}";"
psql "Prices" -c "DROP TABLE "${tickers[$counter]}";"
psql "Prices" -c "ALTER TABLE temp RENAME TO "${tickers[$counter]}";"
psql "Prices" -c "ALTER TABLE "${tickers[$counter]}" SET SCHEMA Prices;"
echo 'cleansing complete for: ' ${tickers[$counter]} 
((counter++))
done
echo "Cleansing C0mplete"
