#!/bin/bash
echo "Building Aggregate Table"
psql "Prices" -c "DROP TABLE Prices.aggregates;"
psql "Prices" -e -c "SELECT * INTO Prices.aggregates FROM Prices.all_indexes;"

tickers=('COPPERCMDUSD' 'GASCMDUSD' 'LIGHTCMDUSD' 'BRENTCMDUSD' 'DIESELCMDUSD' 'XAUUSD' 'XAGUSD' 'GBPUSD' 'NZDUSD' 'USDCAD' 'USDJPY' 'USDCHF' 'USDCAD' 'GBPUSD' 'EURUSD' 'AUDUSD' 'XAGUSD' 'XAUUSD' 'DIESELCMDUSD' 'BRENTCMDUSD' 'LIGHTCMDUSD' 'GASCMDUSD' 'AUDCAD' 'AUDCHF' 'AUDJPY' 'AUDNZD' 'AUDSGD' 'CADCHF' 'CADHKD' 'CADJPY' 'CHFJPY' 'CHFSGD' 'EURAUD' 'EURCAD' 'EURCHF' 'EURCZK' 'EURDKK' 'EURGBP' 'EURHKD' 'EURHUF' 'EURJPY' 'EURNOK' 'EURNZD' 'EURPLN' 'EURRUB' 'EURSEK' 'EURSGD' 'EURTRY' 'GBPAUD' 'GBPCAD' 'GBPCHF' 'GBPJPY' 'GBPNZD' 'HKDJPY' 'NZDCAD' 'NZDCHF' 'NZDJPY' 'SGDJPY' 'TRYJPY' 'USDCNH' 'USDCZK' 'USDDKK' 'USDHUF' 'USDILS' 'USDMXN' 'USDNOK' 'USDPLN' 'USDRON' 'USDRUB' 'USDSEK' 'USDSGD' 'USDTHB' 'USDTRY' 'USDZAR' 'ZARJPY')

counter=0
echo "Indeces COPIED"

while [ $counter -le $(expr ${#tickers[@]} - 1) ]
do
echo 'Inserting: ' ${tickers[$counter]} 'into the Aggregate Table'
psql "Prices" -c "ALTER TABLE Prices.aggregates ADD COLUMN "${tickers[$counter]}"_High NUMERIC, ADD COLUMN "${tickers[$counter]}"_Low NUMERIC, ADD COLUMN "${tickers[$counter]}"_Avg NUMERIC;"
psql "Prices" -c "UPDATE Prices.aggregates AS a SET 
"${tickers[$counter]}"_High = o."${tickers[$counter]}"_high, 
"${tickers[$counter]}"_Low = o."${tickers[$counter]}"_low, 
"${tickers[$counter]}"_Avg = o."${tickers[$counter]}"_avg
FROM (SELECT MAX("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_high, 
	MIN("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_low, 
	AVG("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_avg, CAST(time as date) AS date 
	FROM Prices."${tickers[$counter]}" 
	GROUP BY date) AS o WHERE a.date = o.date;"
# psql "Prices" -c "UPDATE Prices.aggregates SET "${tickers[$counter]}"_High = "${tickers[$counter]}"_days."${tickers[$counter]}"_high FROM 
# 	(SELECT MAX("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_high, 
# 	MIN("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_low, 
# 	AVG("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_avg, CAST(time as date) AS date 
# 	FROM Prices."${tickers[$counter]}" 
# 	GROUP BY date) AS "${tickers[$counter]}"_days;"
# echo "Inserted _High"
# psql "Prices" -c "UPDATE Prices.aggregates SET "${tickers[$counter]}"_Low = "${tickers[$counter]}"_days."${tickers[$counter]}"_low FROM 
# 	(SELECT MAX("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_high, 
# 	MIN("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_low, 
# 	AVG("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_avg, CAST(time as date) AS date 
# 	FROM Prices."${tickers[$counter]}" 
# 	GROUP BY date) AS "${tickers[$counter]}"_days;"
# echo "Inserted _Low"
# psql "Prices" -c "UPDATE Prices.aggregates SET "${tickers[$counter]}"_Avg = "${tickers[$counter]}"_days."${tickers[$counter]}"_avg FROM 
# 	(SELECT MAX("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_high, 
# 	MIN("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_low, 
# 	AVG("${tickers[$counter]}"_bid) AS "${tickers[$counter]}"_avg, CAST(time as date) AS date 
# 	FROM Prices."${tickers[$counter]}" 
# 	GROUP BY date) AS "${tickers[$counter]}"_days;
# 	;"

echo 'penetration complete for: ' ${tickers[$counter]} 
((counter++))
done
echo "Aggregates Gathered"
