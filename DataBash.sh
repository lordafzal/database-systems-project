#!/bin/bash 

echo "XXXXXXXXXXXXXXXXXXFEDERAL RESERVE NOTEXXXXXXXXXXXXXXXXXXX"
echo "XXX  XX       THE UNITED STATES OF AMERICA        XXX  XX"
echo "XXXX XX  -------       ------------               XXXX XX"
echo "XXXX XX              /   jJ===-\    \      C7675  XXXX XX"
echo "XXXXXX     OOO      /   jJ - -  L    \      ---    XXXXXX"
echo "XXXXX     OOOOO     |   JJ  |   X    |       __     XXXXX"
echo "XXX    3   OOO      |   JJ ---  X    |      OOOO    3 XXX"
echo "XXX                 |   J|\    /|    |     OOOOOO     XXX"
echo "XXX     C36799887   |   /  |  |  \   |      OOOO      XXX"
echo "XXX                 |  |          |  |       --       XXX"
echo "XXX      -------    \ /            \ /                XXX"
echo "X  XX                \ ____________ /               X  XX"
echo "XX XXX 3_________        --------  ___   _______ 3 XXX XX"
echo "XX XXX            ___   ONE DOLLAR  i              XXX XX"
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
source env/bin/activate 

v20-pricing-stream -i EUR_USD > euro-dollar-prices.txt
v20-pricing-stream -i GBP_USD > pounds-dollar-prices.txt
v20-pricing-stream -i CAD_USD > canada-dollar-prices.txt
