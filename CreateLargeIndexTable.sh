#!/bin/bash
echo "Building Large Index Table"

psql "Prices" -c "DROP TABLE Prices.all_indexes;"

psql "Prices" -c "CREATE TABLE Prices.all_indexes(date DATE);"
psql "Prices" -c "INSERT INTO Prices.all_indexes(date) SELECT date from Prices.vix;"
tickers=("GSPC" "DJI" "IXIC" "NYA" "XAX" "BUK100P" "RUT" "VIX" "GDAXI" "FCHI" "STOXX50E" "N100" "BFX" "HSI" "AXJO" "JKSE" "NZ50" "KS11" "TWII" "GSPTSE" "BVSP" "MXX" "IPSA" "MERV" "TA125TA" "JN0UJO")

counter=0
while [ $counter -le $(expr ${#tickers[@]} - 1) ]
do
echo 'Inserting: ' ${tickers[$counter]} 'into the Large Index Table'
psql "Prices" -c "ALTER TABLE Prices.all_indexes ADD COLUMN "${tickers[$counter]}"_Open NUMERIC, ADD COLUMN "${tickers[$counter]}"_High NUMERIC, ADD COLUMN "${tickers[$counter]}"_Low NUMERIC, ADD COLUMN "${tickers[$counter]}"_Close NUMERIC, ADD COLUMN "${tickers[$counter]}"_AdjClose NUMERIC, ADD COLUMN "${tickers[$counter]}"_Volume NUMERIC;"
psql "Prices" -c "UPDATE Prices.all_indexes AS a SET "${tickers[$counter]}"_open = o."${tickers[$counter]}"_open, "${tickers[$counter]}"_Close = o."${tickers[$counter]}"_close, "${tickers[$counter]}"_high = o."${tickers[$counter]}"_high, "${tickers[$counter]}"_low = o."${tickers[$counter]}"_low,  "${tickers[$counter]}"_adjclose = o."${tickers[$counter]}"_adjclose, "${tickers[$counter]}"_volume = o."${tickers[$counter]}"_volume FROM Prices."${tickers[$counter]}" AS o WHERE a.date = o.date;"
echo 'penetration complete for: ' ${tickers[$counter]}
((counter++))
done

echo "Large Index Table COMPLETE"
