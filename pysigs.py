import oandapyV20
from oandapyV20 import API
import json
import oandapyV20.endpoints.pricing as pricing
accountID = "101-001-8729002-001"
api = API(access_token="9ef0d6c56b0199e526c98ee7a7510c7a-c8a9541dd0690785de9a040c6d3e6fc0")
params ={"instruments": "EUR_USD,EUR_JPY"}

r = pricing.PricingStream(accountID=accountID, params=params)
rv = api.request(r)
maxrecs = 100
for ticks in rv:
    print json.dumps(ticks, indent=4),","
    if maxrecs == 0:
        r.terminate("maxrecs records received")