from quantopian.pipeline import Pipeline
from quantopian.pipeline.data import morningstar
from quantopian.pipeline.filters.morningstar import Q500US, Q1500US
from quantopian.algorithm import attach_pipeline, pipeline_output
from quantopian.pipeline import CustomFactor

from talib import SMA, TRIMA
import numpy as np
import pandas as pd


def initialize(context):
    
    # Parameters
    context.lookback_days = 150
    context.short_ma_days = 30
    context.long_ma_days = 100
    context.pe_top_cutover = 350
    context.pe_bottom_cutover = -350
    context.num_of_shares = 5
    context.sell_top_rank = 0.9
    context.net_income_cutover = -999999999999
    
    # universe = Q1500US()
    universe = Q1500US() & ( ~Q500US() )
    set_asset_restrictions(security_lists.restrict_leveraged_etfs)
    # End of parameters block 
    
    context.needs_recalculation = True
    
    pe_ratio = morningstar.valuation_ratios.pe_ratio.latest
    net_income = morningstar.cash_flow_statement.net_income.latest
    
    value = ( pe_ratio >= context.pe_bottom_cutover ) & ( pe_ratio <= context.pe_top_cutover ) & ( net_income > context.net_income_cutover )

    pipe = Pipeline(
              columns={
                'pe_ratio': pe_ratio,
                'net_income': net_income,
                'value': value
              },
              screen=(universe)
          )
    attach_pipeline(pipe, name='pe-ratio')

    schedule_function(
        rebalance,
        date_rules.month_start(days_offset=1)
      )

    schedule_function(
        schedule_rebalance,
        date_rules.month_start(days_offset=0)
      )
    # Set commision model for IB
    set_commission(commission.PerShare(cost=0.005, min_trade_cost=1.00))
    
def handle_data(context, data):
    pass


def before_trading_start(context, data):
    
    if not context.needs_recalculation:
        return
    
    # Ensure there is no unneeded recalcs
    context.needs_recalculation = False
    
    pe_ratio_result = pipeline_output('pe-ratio')
    
    symbols = pe_ratio_result.index.get_level_values(0)

    closes = data.history(symbols, fields='close', bar_count=context.lookback_days, frequency='1d')

    ranking = {}

    for sec in closes.columns:
        prices = closes.loc[:, sec].values
        sma = SMA(prices, timeperiod=context.long_ma_days)
        trima=TRIMA(prices, timeperiod=context.short_ma_days)
        nd_days = np.where(trima > sma, 1, 0)
        days = pd.Series(nd_days)
        c_days = days * (days.groupby((days != days.shift(1)).cumsum()).cumcount() + 1)
        ranking[sec] = c_days.values[-1] * (trima[-1] / sma[-1])

    context.values_df = pd.DataFrame.from_dict(data=ranking, orient='index')
    
    context.values_df.columns = ['myrsi']
    context.values_df['rank'] = context.values_df.rank(pct=True, na_option='bottom').round(decimals=2)
    context.values_df['value'] = pe_ratio_result['value']

    
# Rebalancing logic
def schedule_rebalance(context, data):
    record("Cash", context.portfolio.cash)
    record("Num of shares", len(context.portfolio.positions))
    context.needs_recalculation = True

def rebalance(context, data):
    
    def get_top(n):
        return context.values_df[context.values_df['value']].nlargest(n, columns=['myrsi']).index.values

    def get_rank(security):
        return context.values_df.loc[security]['rank']

    def exists(security):
        return security in context.values_df.index

    
    # Is there anything to sell?
    if len(context.portfolio.positions) > 0:
        for security in context.portfolio.positions:
            
            if not exists(security):
                log.info("Selling bogus %s" % security)
                order_target_percent(security, 0)
            else:    
                rank = get_rank(security)
                if (rank <= context.sell_top_rank) and data.can_trade(security):
                    log.info("Selling %s" % security)
                    order_target_percent(security, 0)
    
    needed = context.num_of_shares - len(context.portfolio.positions)
    cash = context.portfolio.cash
    
    # Do we need to buy?
    if needed > 0:
        new_securities = get_top(needed)

        for security in new_securities:
            if data.can_trade(security):
                log.info("Buying %s" % security)
                #order_target_percent(security, (1. / context.num_of_shares))
                order_target_value(security, (round(cash / needed)))
