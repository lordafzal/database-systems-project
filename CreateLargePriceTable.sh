#!/bin/bash
echo "Building Large Prices Table"

psql "Prices" -c "DROP TABLE Prices.all_prices;"
psql "Prices" -c "CREATE TABLE all_prices(time TIMESTAMP);"

tickers=('USDJPY' 'USDCHF' 'USDCAD' 'NZDUSD' 'GBPUSD' 'EURUSD' 'AUDUSD' 'XAGUSD' 'XAUUSD' 'DIESELCMDUSD' 'BRENTCMDUSD' 'LIGHTCMDUSD' 'GASCMDUSD' 'COPPERCMDUSD' 'AUDCAD' 'AUDCHF' 'AUDJPY' 'AUDNZD' 'AUDSGD' 'CADCHF' 'CADHKD' 'CADJPY' 'CHFJPY' 'CHFSGD' 'EURAUD' 'EURCAD' 'EURCHF' 'EURCZK' 'EURDKK' 'EURGBP' 'EURHKD' 'EURHUF' 'EURJPY' 'EURNOK' 'EURNZD' 'EURPLN' 'EURRUB' 'EURSEK' 'EURSGD' 'EURTRY' 'GBPAUD' 'GBPCAD' 'GBPCHF' 'GBPJPY' 'GBPNZD' 'HKDJPY' 'NZDCAD' 'NZDCHF' 'NZDJPY' 'SGDJPY' 'TRYJPY' 'USDCNH' 'USDCZK' 'USDDKK' 'USDHDK' 'USDHUF' 'USDILS' 'USDMXN' 'USDNOK' 'USDPLN' 'USDRON' 'USDRUB' 'USDSEK' 'USDSGD' 'USDTHB' 'USDTRY' 'USDZAR' 'ZARJPY')

counter=0
while [ $counter -le $(expr ${#tickers[@]} - 1) ]
do
echo 'Inserting: ' ${tickers[$counter]} 'into the Large Price Table'
psql "Prices" -c "ALTER TABLE Prices.all_prices ADD COLUMN "${tickers[$counter]}"_Bid NUMERIC, ADD COLUMN "${tickers[$counter]}"_Ask NUMERIC, ADD COLUMN "${tickers[$counter]}"_volBid NUMERIC, ADD COLUMN "${tickers[$counter]}"_volAsk NUMERIC;"
psql "Prices" -c "INSERT INTO Prices.all_prices (time, "${tickers[$counter]}"_Bid, "${tickers[$counter]}"_ASK, "${tickers[$counter]}"_volBid, "${tickers[$counter]}"_volAsk) SELECT DISTINCT * FROM Prices."${tickers[$counter]}";"
echo 'penetration complete for: ' ${tickers[$counter]}
((counter++))
done
