#!/usr/bin/python

import networkx as nx
import math
import sys, os, glob, re, time
import numpy as np
import csv
from collections import Counter
import pickle
import gc
import datetime
import pandas as pd
import webbrowser
from networkx.algorithms import community as newComms
import datetime
from datetime import date, timedelta
from dateutil import parser
import subprocess
import psycopg2
from sshtunnel import SSHTunnelForwarder
import community

def shrinkData(df):
    df_int = df.select_dtypes(include=['int'])
    df_int = df_int.apply(pd.to_numeric,downcast='unsigned')
    df_float = df.select_dtypes(include=['float'])
    df_float = df_float.apply(pd.to_numeric,downcast='float')
    df['date'] = pd.to_datetime(df['date'])
    cols = df.columns[df.dtypes.eq(object)]
    df[cols] = df[cols].apply(pd.to_numeric, errors='coerce', axis=0)
    return df

def shrinKData(df):
    df_int = df.select_dtypes(include=['int'])
    df_int = df_int.apply(pd.to_numeric,downcast='unsigned')
    df_float = df.select_dtypes(include=['float'])
    df_float = df_float.apply(pd.to_numeric,downcast='float')
    df['time'] = pd.to_datetime(df['time'])
    cols = df.columns[df.dtypes.eq(object)]
    df[cols] = df[cols].apply(pd.to_numeric, errors='coerce', axis=0)
    return df

def query_to_DataFrame(sqlString):
    with SSHTunnelForwarder(
        ('24.228.50.99', 443),
        #ssh_private_key="</path/to/private/ssh/key>",
        ### in my case, I used a password instead of a private key
        ssh_username="root",
        ssh_password="b3110cFr13nd", 
        remote_bind_address=('localhost', 5432)) as server:

        server.start()
        print("server connected")

        params = {
         'database': 'Prices',
         'user': 'explorer',
         'password': 's0j0urn3r',
         'host': 'localhost',
         'port': server.local_bind_port
         }

        conn = psycopg2.connect(**params)
        curs = conn.cursor()
        print("database connected")
        # Get a database cursor
        cur = conn.cursor()

        # Execute SQL
        cur.execute(sqlString)
        result = cur.fetchall()
        colnames = [desc[0] for desc in cur.description]
        frame = shrinkData(pd.DataFrame(result, columns=colnames))

        # Close connections
        conn.close()

        # Stop the tunnel
        server.stop()
        return frame
    
def Raw_query_to_DataFrame(sqlString):
    with SSHTunnelForwarder(
        ('24.228.50.99', 443),
        #ssh_private_key="</path/to/private/ssh/key>",
        ### in my case, I used a password instead of a private key
        ssh_username="root",
        ssh_password="b3110cFr13nd", 
        remote_bind_address=('localhost', 5432)) as server:

        server.start()
        print("server connected")

        params = {
         'database': 'Prices',
         'user': 'explorer',
         'password': 's0j0urn3r',
         'host': 'localhost',
         'port': server.local_bind_port
         }

        conn = psycopg2.connect(**params)
        curs = conn.cursor()
        print("database connected")
        # Get a database cursor
        cur = conn.cursor()

        # Execute SQL
        cur.execute(sqlString)
        # Get the result
        result = cur.fetchall()
        colnames = [desc[0] for desc in cur.description]
        frame = shrinKData(pd.DataFrame(result, columns=colnames))
    #     print(result)

        # Close connections
        conn.close()

        # Stop the tunnel
        server.stop()
        return frame
    
def merge_data(a, b, from_date='2018-01-01'):
    """
    a: first DataFrame
    b: second DataFrame
    from_date: includes the data from the provided date and drops the any data before that date.
    returns merged data as Pandas DataFrame
    """
    merged_data = pd.merge(a, b, on=['Date'])
    merged_data = merged_data[merged_data['Date'] >= from_date]
    print("Merged")
    return merged_data
# reads tsv edge list where each line is in the format "leftnode rightnode"
# assumes file header contains 'sym' for undirected graph and 'asym' for directed graphs
def convert_tsv_to_graphml(filename):
    m = re.search("(.*)\.tsv", filename)
    name = m.group(1)
    file = open(filename, 'rb')
    header = file.readline()
    print(header)
    if b"asym" in header:
        graph_type = nx.DiGraph()
    else:
        graph_type = nx.Graph()
    lines = [line for line in file if not line.startswith(b"%")]
    print(lines[0], lines[1],"\n")
    g = nx.read_edgelist(lines, create_using=graph_type)
    nx.write_graphml(g, name+".graphml")  
    print(filename)
    return g

def distribution(counts, attribute):
    vals, cnt = zip(*counts.items())
    
    # get rid of all negative values by bumping everything up
    if (np.array(vals) < 0).any():
        print(attribute + " had negative values but is shifted up by {} now".format(-min(vals) + 1))
        vals = np.array(vals) - min(vals) + 1
        
    if 0 in np.array(vals):
        vals = np.array(vals) + 1   # account for possible 0 values for log plot
        print(attribute + " has been shifted up by 1")
        
    fig, ax = plt.subplots()
    plt.scatter(vals, cnt, color='b')
    
    plt.title(attribute + " Plot")
    plt.ylabel("Count")
    plt.xlabel(attribute)
    plt.yscale('log')
    # clustering coefficient only has log plot, not log log
    if attribute == "Clustering Coefficient":
        plt.xscale('linear')
        log_vals = np.log10(vals)
    else:
        plt.xscale('log')
        log_vals = np.log10(vals)
    
    #deg = np.log10(np.array(deg))
    #cnt = np.log10(np.array(cnt))
    vals = np.array(vals)
    cnt = np.array(cnt)
    log_vals = np.log10(vals)
    log_cnt = np.log10(cnt)
    
    m, b = np.polyfit(log_vals, log_cnt, 1)
    y_fit = np.power(10, m*log_vals + b)
    #plt.plot(deg, cnt, '.')
    plt.plot(vals, y_fit , '-')
    
# This will read in the correlation TSV file and create the graphML file to disk
def make_currency_graph(filename, name):
    corr = np.genfromtxt(fname=filename, delimiter='\t',dtype=float, names=True)    # fname="correlations.tsv"
    corrGraph = nx.Graph()
    names = corr.dtype.names
    
    for i in range(len(corr)):
        l_node = names[i]
        l_type = l_node.split("_")[-1]
        l_name = l_node.split("_")[0]
        
        for j in range(i + 1, len(corr)):
            r_node = names[j]
            
            if i == 0:
                r_type = r_node.split("_")[-1]
                r_name = r_node.split("_")[0]
                corrGraph.add_node(l_node, position=l_type, name = l_name)
                corrGraph.add_node(r_node, position=r_type, name = r_name)
                
            corrGraph.add_edge(l_node, r_node, weight = float(corr[i][j]))
    
    corrGraph.name = name
    corrGraph = nx.maximum_spanning_tree(corrGraph)
    nx.write_graphml(corrGraph, corrGraph.name + ".graphml")

    return corrGraph


print("$$$$$$$$\\                       $$\\           ")
print("\\__$$  __|                      $$ |          ")
print("   $$ | $$$$$$\\  $$$$$$\\   $$$$$$$ | $$$$$$\\  ")
print("   $$ |$$  __$$\\ \\____$$\\ $$  __$$ |$$  __$$\\ ")
print("   $$ |$$ |  \\__|$$$$$$$ |$$ /  $$ |$$$$$$$$ |")
print("   $$ |$$ |     $$  __$$ |$$ |  $$ |$$   ____|")
print("   $$ |$$ |     \\$$$$$$$ |\\$$$$$$$ |\\$$$$$$$\\ ")
print( "   \\__|\\__|      \\_______| \\_______| \\_______|")
print("                                              ")


# This will read in the correlation TSV file and create the graphML file to disk
def process_currency_graph(filename, name, typ, threshold = 0):
    corr = np.genfromtxt(fname=filename, delimiter='\t',dtype=float, names=True)    # fname="correlations.tsv"
    corrGraph = nx.Graph()
    names = corr.dtype.names
    lst = list(names)
    for i in range(len(lst)):
        lst[i] = lst[i].split("_")[0]
    names = tuple(lst)
    
    for i in range(len(corr)):
        l_node = names[i]
        l_type = l_node.split("_")[-1]
        l_name = l_node.split("_")[0]
        
        for j in range(i + 1, len(corr)):
            r_node = names[j]
            
            if i == 0:
                r_type = r_node.split("_")[-1]
                r_name = r_node.split("_")[0]
                corrGraph.add_node(l_node, position=l_type, name = l_name)
                corrGraph.add_node(r_node, position=r_type, name = r_name)
            
            if float(corr[i][j]) >= threshold or float(corr[i][j]) <= -threshold:
                corrGraph.add_edge(l_node, r_node, weight = float(corr[i][j]))
    
    corrGraph.name = name
    return corrGraph


def createGraphFiles(filename, name):
    graphs = []
    g = make_currency_graph(filename, name)
    print("Creating graphml file for", g.name)
    print("Graph name is", g.name)

    # Create or retrieve Erdos-Renyi random graph
    if not os.path.exists(g.name + "_ER.graphml"):
        print("Creating ER graph for " + g.name + " from scratch")
        k = 2 * g.number_of_edges() / g.number_of_nodes()   # average degree

 
        print("For currencies, use weighted degree for ER")
        edges_dict = g.edges()
        k = sum([edges_dict[edge]["weight"] for edge in edges_dict]) * 2 / g.number_of_nodes()

        p = k / (g.number_of_nodes() - 1)                  # probability = avg deg / # of possible neighbors (n-1)
        g_ER = nx.generators.erdos_renyi_graph(g.number_of_nodes(), p)
        nx.write_graphml(g_ER, g.name+"_ER.graphml")
    else:
        print("Reading in the ER graphml file for " + g.name)
        g_ER = nx.read_graphml(g.name+"_ER.graphml")
    g_ER.name = g.name + "_ER"

    # Create or retrieve Barabasi-Albert scale-free network
    if not os.path.exists(g.name + "_BA.graphml"):
        print("Creating BA graph for " + g.name + " from scratch")
        m = 1
        n = g.number_of_nodes()
        g_BA = nx.generators.barabasi_albert_graph(n, m)
        old_dist = abs(g.number_of_edges() - g_BA.number_of_edges())

        # increment m until it generates the BA graph closest to real graph also make sure m never equals n
        while g_BA.number_of_edges() < g.number_of_edges() and m + 1 < n:
            m += 1
            old_dist = abs(g.number_of_edges() - g_BA.number_of_edges())
            g_BA = nx.generators.barabasi_albert_graph(n, m)

        # See how close our graph is to the original and decrement m by 1 if we've gone too far    
        dist = abs(g_BA.number_of_edges() - g.number_of_edges())
        if old_dist < dist:
            g_BA = nx.generators.barabasi_albert_graph(n, m - 1)
            print("Use previous m value:", m - 1)
        else:
            print("Use current m value:", m)

        nx.write_graphml(g_BA, g.name + "_BA.graphml")
    else:
        print("Reading in the BA graphml file for " + g.name)
        g_BA = nx.read_graphml(g.name+"_BA.graphml")
    g_BA.name = g.name + "_BA"    

    graphs += [g, g_ER, g_BA]
    return graphs

def analyzeGraphs(graphs):
    # Now loop through all of our graphs
    i = 1
    for g in graphs:

        graph_name = g.name

        # if we haven't generated the CSV files yet, cut the program off and prompt the user to do so
        if not os.path.exists(graph_name + ".csv"):
            print("We are missing the CSV file of statistics '{}'".format(graph_name + ".csv"))
            print("Now run Gephi statistics on the created GraphML files and export as CSV, then rerun this program")
            continue

        print("\nReading the graph metrics from '" + graph_name + "'.csv")

        statFilCSV = open(graph_name + ".csv", 'r')
        headFile = open(graph_name + ".csv", 'r')
        header = next(csv.reader(headFile))
        headFile.close()

        reader = csv.DictReader(statFilCSV)
        network_stats = {attribute: [] for attribute in header}
        counters = {}

        # Get raw network statistics from CSV Gephi output
        for row in reader:
            Id, label, timeset, eccentricity, closnesscentrality, harmonicclosnesscentrality, betweenesscentrality, componentnumber, degree, clustering, triangles, eigencentrality, weightedDegree             = row["Id"], row["Label"],  row["timeset"], int(row["Eccentricity"]),   float(row["closnesscentrality"]),   float(row["harmonicclosnesscentrality"]),   float(row["betweenesscentrality"]), int(row["componentnumber"]),    int(row["Degree"]), float(row["clustering"]),   int(row["triangles"]),  float(row["eigencentrality"]),  float(row["Weighted Degree"])
            network_stats["clustering"].append(clustering)
            network_stats["betweenesscentrality"].append(betweenesscentrality)
            network_stats["Degree"].append(degree)
            network_stats["closnesscentrality"].append(closnesscentrality)
            network_stats["componentnumber"].append(componentnumber)
            network_stats["Weighted Degree"].append(weightedDegree)

        # Get distribution of network statistics
        counters["Degree"] = Counter(sorted(network_stats["Degree"]))
        counters["clustering"] = Counter(sorted(network_stats["clustering"]))
        counters["betweenesscentrality"] = Counter(sorted(network_stats["betweenesscentrality"]))
        counters["closnesscentrality"] = Counter(sorted(network_stats["closnesscentrality"]))
        counters["componentnumber"] = Counter(sorted(network_stats["componentnumber"]))
        counters["Weighted Degree"] = Counter(sorted(network_stats["Weighted Degree"]))

        components = nx.connected_component_subgraphs(g)
        components = sorted(components, key=len, reverse=True)
        network_stats["componentsize"] = [len(comp) for comp in components]
        cc = Counter(network_stats["componentsize"])
        gc = components[0]
        print("Size of largest connected component:", len(gc))
        counters["componentsize"] = cc

        plt.figure(i)
        i += 1

        distribution(counters["Degree"], "Degree")
        plt.suptitle(graph_name)
        if not os.path.exists(graph_name+"-Degree.png"):
            plt.savefig(graph_name+"-Degree.png")
            print("Saving {}-Degree.png".format(graph_name))

        if "currencyCorrelations" in graph_name:
            distribution(counters["Weighted Degree"], "Weighted Degree")
            plt.suptitle(graph_name)
            if not os.path.exists(graph_name+"-WeightedDegree.png"):
                plt.savefig(graph_name+"-WeightedDegree.png")
                print("Saving {}-WeightedDegree.png".format(graph_name))

        if len(set(network_stats["clustering"])) > 1:
            distribution(counters["clustering"], "Clustering Coefficient")
            plt.suptitle(graph_name)
            if not os.path.exists(graph_name+"-ClusteringCoefficient.png"):
                plt.savefig(graph_name+"-ClusteringCoefficient.png")
                print("Saving {}-ClusteringCoefficient.png".format(graph_name))

        if len(set(network_stats["betweenesscentrality"])) > 1:
            distribution(counters["betweenesscentrality"], "Betweeness Centrality")
            plt.suptitle(graph_name)
            if not os.path.exists(graph_name+"-BetweenessCentrality.png"):
                plt.savefig(graph_name+"-BetweenessCentrality.png")
                print("Saving {}-BetweenessCentrality.png".format(graph_name))

        if len(set(network_stats["componentnumber"])) > 1:
            distribution(counters["componentsize"], "Connected Components Size")
            plt.suptitle(graph_name)
            if not os.path.exists(graph_name+"-ConnectedComponents.png"):
                plt.savefig(graph_name+"-ConnectedComponents.png")
                print("Saving {}-ConnectedComponents.png".format(graph_name))

        plt.show()

        print("\nAverage Values:")
        print("\tAverage Degree: {} weighted, {} unweighted".format(np.mean(network_stats["Degree"]), np.mean(network_stats["Weighted Degree"])))
        print("\tAverage Clustering Coefficient:", np.mean(network_stats["clustering"]))
        print("\tAverage Betweeness Centrality:", np.mean(network_stats["betweenesscentrality"]))
        print("\tAverage Connected Components Size:", np.mean(network_stats["componentsize"]))     # since there's only one connected component, average is # of nodes

        print("\nVariance of Values:")
        print("\tVariance of Degree: {} weighted, {} unweighted".format(np.var(network_stats["Degree"]), np.var(network_stats["Weighted Degree"])))
        print("\tVariance of Clustering Coefficient:", np.var(network_stats["clustering"]))
        print("\tVariance of Betweeness Centrality:", np.var(network_stats["betweenesscentrality"]))
        print("\tVariance of Connected Components Size:", np.var(network_stats["componentsize"]))     # since there's only one connected component, variance is 0


        print("Average Closeness Centrality:", np.mean(network_stats["closnesscentrality"]))
        statFilCSV.close()
        
##### This function draws an induced graph
def draw_induced_graph(G, part):
    g = G
    ind = community.induced_graph(part, g)
    values = [part.get(node) for node in G.nodes()]
    plt.figure(figsize=(25,25))
    nx.draw_spring(G, cmap = plt.get_cmap('jet'), node_color = values, node_size=200, with_labels=True)

def graphinate(dataframe, name):
    correlations = dataframe.corr()
    correlations.to_csv('correlations.tsv', sep='\t', encoding='utf-8', index=False)
    G = make_currency_graph('correlations.tsv', name)
    return G

def graphinatePrime(dataframe, name, typ, threshold):
    correlations = dataframe.corr()
    correlations.to_csv('correlations.tsv', sep='\t', encoding='utf-8', index=False)
    G = process_currency_graph('correlations.tsv', name, typ, threshold)
    return G

def get_market_data(market, tag=True):
    market_data = pd.read_html("https://coinmarketcap.com/currencies/" + market + 
        "/historical-data/?start=20180101&end="+time.strftime("%Y%m%d"), flavor='html5lib')[0]
    market_data = market_data.rename(columns={'Date':'date'})
    market_data = market_data.assign(Date=pd.to_datetime(market_data['date']))
    market_data['Volume'] = (pd.to_numeric(market_data['Volume'], errors='coerce').fillna(0))
    if tag:
        market_data.columns = [market_data.columns[0]] + [tag + '_' + i for i in market_data.columns[1:]]
    return market_data

def convert_tuple(tuple_partition):
    retDict = dict()
    for node in tuple_partition[0]:
        retDict.update({node:0})
    for node in tuple_partition[1]:
        retDict.update({node:1})
    return retDict

def convert_sort(sorted_list):
    retDict = dict()
    for i in range(0,len(sorted_list)):
        for name in sorted_list[i]:
            retDict.update({name:i})
    return retDict




#### This Cell pulls in the data from the database
aggregateQuery = "SELECT * FROM Prices.aggregates WHERE date >= '2016-01-01';"
aggregateframe = query_to_DataFrame(aggregateQuery)

indexes = ['GSPC', 'DJI', 'IXIC', 'NYA', 'XAX', 'BUK100P', 'RUT', 'VIX', 'GDAXI', 'FCHI', 'STOXX50E', 'N100', 'BFX', 'HSI', 'AXJO', 'JKSE', 'NZ50', 'KS11', 'TWII', 'GSPTSE', 'BVSP', 'MXX', 'IPSA', 'MERV', 'TA125TA', 'JN0UJO']
instruments = ['COPPERCMDUSD', 'GASCMDUSD', 'LIGHTCMDUSD', 'BRENTCMDUSD', 'DIESELCMDUSD', 'XAUUSD', 'XAGUSD', 'GBPUSD', 'NZDUSD', 'USDCAD', 'USDJPY', 'USDCHF', 'USDCAD', 'GBPUSD', 'EURUSD', 'AUDUSD', 'XAGUSD', 'XAUUSD', 'DIESELCMDUSD', 'BRENTCMDUSD', 'LIGHTCMDUSD', 'GASCMDUSD', 'AUDCAD', 'AUDCHF', 'AUDJPY', 'AUDNZD', 'AUDSGD', 'CADCHF', 'CADHKD', 'CADJPY', 'CHFJPY', 'CHFSGD', 'EURAUD', 'EURCAD', 'EURCHF', 'EURCZK', 'EURDKK', 'EURGBP', 'EURHKD', 'EURHUF', 'EURJPY', 'EURNOK', 'EURNZD', 'EURPLN', 'EURRUB', 'EURSEK', 'EURSGD', 'EURTRY', 'GBPAUD', 'GBPCAD', 'GBPCHF', 'GBPJPY', 'GBPNZD', 'HKDJPY', 'NZDCAD', 'NZDCHF', 'NZDJPY', 'SGDJPY', 'TRYJPY', 'USDCNH', 'USDCZK', 'USDDKK', 'USDHUF', 'USDILS', 'USDMXN', 'USDNOK', 'USDPLN', 'USDRON', 'USDRUB', 'USDSEK', 'USDSGD', 'USDTHB', 'USDTRY', 'USDZAR', 'ZARJPY']
nameList = ['Dates']
instruments = list(set(instruments))
indexes = list(set(indexes))

### Separate Stocks and Currencies

indexSection = (len(indexes) * 6)+1
StockIndexes = aggregateframe

StockIndexes = StockIndexes.drop(columns=aggregateframe.columns[indexSection:])
# StockIndexes.head()

currencies = aggregateframe
currencies = currencies.drop(columns=aggregateframe.columns[1:indexSection])
# currencies.head()

## Organize Currency Data

a = currencies.columns[currencies.columns.str.endswith('avg')]
a = a.insert(0,'date')
averages = currencies[a]
# averages.head()

a = currencies.columns[currencies.columns.str.endswith('high')]
a = a.insert(0,'date')
highCurr = currencies[a]
# highCurr.head()

a = currencies.columns[currencies.columns.str.endswith('low')]
a = a.insert(0,'date')
lowCurr = currencies[a]
# lowCurr.head()

### Organize Stock Indexes

a = StockIndexes.columns[StockIndexes.columns.str.endswith('open')]
a = a.insert(0,'date')
opens = StockIndexes[a]
# opens.head()

a = StockIndexes.columns[StockIndexes.columns.str.endswith('close')]
a = a.insert(0,'date')
closes = StockIndexes[a]
# closes.head()

a = StockIndexes.columns[StockIndexes.columns.str.endswith('high')]
a = a.insert(0,'date')
highStk = StockIndexes[a]
# highStk.head()

a = StockIndexes.columns[StockIndexes.columns.str.endswith('low')]
a = a.insert(0,'date')
lowStk = StockIndexes[a]
# lowStk.head()

a = StockIndexes.columns[StockIndexes.columns.str.endswith('adjclose')]
a = a.insert(0,'date')
adjclose = StockIndexes[a]
# opens.head()

a = StockIndexes.columns[StockIndexes.columns.str.endswith('volume')]
a = a.insert(0,'date')
volumes = StockIndexes[a]
# volumes.head()

### Some Aggregates
a = aggregateframe.columns[aggregateframe.columns.str.endswith('high')]
a = a.insert(0,'date')
highAll = aggregateframe[a]
# highAll.head()

a = aggregateframe.columns[aggregateframe.columns.str.endswith('low')]
a = a.insert(0,'date')
lowAll = aggregateframe[a]
# lowAll.head()

### Get and Organize CryptoCurrency Data


cointags = [ ('bitcoin', 'BTC'), ('ripple', 'XRP'), ('ethereum', 'ETH'), ('stellar', 'XLM'), 
           ('eos', 'EOS'), ('litecoin', 'LTC'), ('tether', 'USDT'), ('cardano', 'ADA'), ('monero', 'XMR'),
            ('tron', 'TRX'), ('iota', 'MIOTA'), ('dash', 'DASH'), ('bitcoin-cash', 'BCH'), ('binance-coin','BNB')]
coinframes =[]
for tag in cointags:
    print(tag)
    frame = shrinkData(get_market_data(tag[0],tag[1]))
    frame = frame.drop(columns=tag[1]+'_Date')
    coinframes.append(frame)
    coinframes[-1].rename(columns={tag[1]+'_Open*': tag[1]+'_Open', tag[1]+'_Close**':tag[1]+'_Close'}, inplace=True)
    
coins = coinframes[0]
for i in range(1, len(coinframes)):
    coins = pd.merge(coins, coinframes[i], on=['date'])
    

a = coins.columns[coins.columns.str.endswith('Open')]
a = a.insert(0,'date')
opensCoins = coins[a]
# opensCoins.head()

a = coins.columns[coins.columns.str.endswith('Close')]
a = a.insert(0,'date')
closesCoins = coins[a]
# closesCoins.head()

a = coins.columns[coins.columns.str.endswith('High')]
a = a.insert(0,'date')
highCoins = coins[a]
# highCoins.head()

a = coins.columns[coins.columns.str.endswith('Low')]
a = a.insert(0,'date')
lowCoins = coins[a]
# lowCoins.head()


a = coins.columns[coins.columns.str.endswith('Volume')]
a = a.insert(0,'date')
volumesCoins = coins[a]
# volumesCoins.head()

### Create Mixed Market Data Frames

all_Highs = pd.merge(highStk, highCurr, on=['date'])
all_Highs = pd.merge(all_Highs, highCoins, on=['date'])

all_Lows = pd.merge(lowStk, lowCurr, on=['date'])
all_Lows = pd.merge(all_Lows, lowCoins, on=['date'])


def chkBuy(instrument, signals, graph, pos_threshold, neg_threshold, threshold):
    nbrs = graph[instrument]
    edges = graph.edges.data("weight", nbunch = instrument)
    matches = 0
    for edge in edges:
        if edge[2] > pos_threshold and signals[edge[1]] == "up" or edge[2] < neg_threshold and signals[edge[1]] == "down":
            matches += 1
    print(str(matches) +  "out of "+str(len(edges))+"edges")
    confidence = matches / len(edges)
    print("confidence = "+str(confidence))
    if confidence >= threshold:
        return True
    else:
        return False
    
def chkSell(instrument, signals, graph, pos_threshold, neg_threshold, threshold):
    nbrs = graph[instrument]
    edges = graph.edges.data("weight", nbunch = instrument)
    matches = 0
    for edge in edges:
        if edge[2] > pos_threshold and signals[edge[1]] == "down" or edge[2] < neg_threshold and signals[edge[1]] == "up":
            matches += 1
    print(str(matches) +  "out of "+str(len(edges))+"edges")
    confidence = matches / len(edges)
    print("confidence = "+str(confidence))
    if confidence >= threshold:
        return True
    else:
        return False


treated_avg = averages.drop(columns=['coppercmdusd_avg', 'gascmdusd_avg', 'lightcmdusd_avg', 'brentcmdusd_avg','dieselcmdusd_avg'])
treated_high = highCurr.drop(columns=['coppercmdusd_high', 'gascmdusd_high', 'lightcmdusd_high', 'brentcmdusd_high','dieselcmdusd_high'])
treated_low = lowCurr.drop(columns=['coppercmdusd_low', 'gascmdusd_low', 'lightcmdusd_low', 'brentcmdusd_low','dieselcmdusd_low'])


def MakeBuys(frame, pos_threshold, neg_threshold, threshold):
    instruments = ['XAUUSD', 'XAGUSD', 'GBPUSD', 'NZDUSD', 'USDCAD', 'USDJPY', 'USDCHF', 'USDCAD', 'GBPUSD', 'EURUSD', 'AUDUSD', 'XAGUSD', 'XAUUSD', 'AUDCAD', 'AUDCHF', 'AUDJPY', 'AUDNZD', 'AUDSGD', 'CADCHF', 'CADHKD', 'CADJPY', 'CHFJPY', 'CHFSGD', 'EURAUD', 'EURCAD', 'EURCHF', 'EURCZK', 'EURDKK', 'EURGBP', 'EURHKD', 'EURHUF', 'EURJPY', 'EURNOK', 'EURNZD', 'EURPLN', 'EURRUB', 'EURSEK', 'EURSGD', 'EURTRY', 'GBPAUD', 'GBPCAD', 'GBPCHF', 'GBPJPY', 'GBPNZD', 'HKDJPY', 'NZDCAD', 'NZDCHF', 'NZDJPY', 'SGDJPY', 'TRYJPY', 'USDCNH', 'USDCZK', 'USDDKK', 'USDHUF', 'USDILS', 'USDMXN', 'USDNOK', 'USDPLN', 'USDRON', 'USDRUB', 'USDSEK', 'USDSGD', 'USDTHB', 'USDTRY', 'USDZAR', 'ZARJPY']
    instruments = list(set(instruments))
    siggies = getSignals()
    retList = []
    for ticker in instruments:
        ticker = ticker.lower()
        G = graphinatePrime(frame, ticker, 'max', 0)
        chk = chkBuy(ticker, siggies, G, pos_threshold, neg_threshold, threshold)
        print(ticker+" signal evaluates to "+str(chk))
        if chk == True:
            retList.append(ticker)
                    
    return retList

def MakeSells(frame, pos_threshold, neg_threshold, threshold):
    instruments = ['XAUUSD', 'XAGUSD', 'GBPUSD', 'NZDUSD', 'USDCAD', 'USDJPY', 'USDCHF', 'USDCAD', 'GBPUSD', 'EURUSD', 'AUDUSD', 'XAGUSD', 'XAUUSD', 'AUDCAD', 'AUDCHF', 'AUDJPY', 'AUDNZD', 'AUDSGD', 'CADCHF', 'CADHKD', 'CADJPY', 'CHFJPY', 'CHFSGD', 'EURAUD', 'EURCAD', 'EURCHF', 'EURCZK', 'EURDKK', 'EURGBP', 'EURHKD', 'EURHUF', 'EURJPY', 'EURNOK', 'EURNZD', 'EURPLN', 'EURRUB', 'EURSEK', 'EURSGD', 'EURTRY', 'GBPAUD', 'GBPCAD', 'GBPCHF', 'GBPJPY', 'GBPNZD', 'HKDJPY', 'NZDCAD', 'NZDCHF', 'NZDJPY', 'SGDJPY', 'TRYJPY', 'USDCNH', 'USDCZK', 'USDDKK', 'USDHUF', 'USDILS', 'USDMXN', 'USDNOK', 'USDPLN', 'USDRON', 'USDRUB', 'USDSEK', 'USDSGD', 'USDTHB', 'USDTRY', 'USDZAR', 'ZARJPY']
    instruments = list(set(instruments))
    siggies = getSignals()
    retList = []
    for ticker in instruments:
        ticker= ticker.lower()
        try:
            G = graphinatePrime(frame, ticker, 'max', 0)
            chk = chkSell(ticker, siggies, G, pos_threshold, neg_threshold, threshold)
            print(ticker+" signal evaluates to "+str(chk))
            if chk == True:
                retList.append(ticker)
        except:
            print(ticker+" failed")
    return retList

def LiteBuys(frame, signals, pos_threshold, neg_threshold, threshold):
    retList = []
    instruments = ['XAUUSD', 'XAGUSD', 'GBPUSD', 'NZDUSD', 'USDCAD', 'USDJPY', 'USDCHF', 'USDCAD', 'GBPUSD', 'EURUSD', 'AUDUSD', 'XAGUSD', 'XAUUSD', 'AUDCAD', 'AUDCHF', 'AUDJPY', 'AUDNZD', 'AUDSGD', 'CADCHF', 'CADHKD', 'CADJPY', 'CHFJPY', 'CHFSGD', 'EURAUD', 'EURCAD', 'EURCHF', 'EURCZK', 'EURDKK', 'EURGBP', 'EURHKD', 'EURHUF', 'EURJPY', 'EURNOK', 'EURNZD', 'EURPLN', 'EURRUB', 'EURSEK', 'EURSGD', 'EURTRY', 'GBPAUD', 'GBPCAD', 'GBPCHF', 'GBPJPY', 'GBPNZD', 'HKDJPY', 'NZDCAD', 'NZDCHF', 'NZDJPY', 'SGDJPY', 'TRYJPY', 'USDCNH', 'USDCZK', 'USDDKK', 'USDHUF', 'USDILS', 'USDMXN', 'USDNOK', 'USDPLN', 'USDRON', 'USDRUB', 'USDSEK', 'USDSGD', 'USDTHB', 'USDTRY', 'USDZAR', 'ZARJPY']
    instruments = list(set(instruments))
    for ticker in instruments:
        ticker = ticker.lower()
        G = graphinatePrime(frame, ticker, 'max', 0)
        chk = chkBuy(ticker, signals, G, pos_threshold, neg_threshold, threshold)
        print(ticker+" signal evaluates to "+str(chk))
        if chk == True:
            retList.append(ticker)                 
    return retList

def LiteSells(frame, signals, pos_threshold, neg_threshold, threshold):
    retList = []
    instruments = ['XAUUSD', 'XAGUSD', 'GBPUSD', 'NZDUSD', 'USDCAD', 'USDJPY', 'USDCHF', 'USDCAD', 'GBPUSD', 'EURUSD', 'AUDUSD', 'XAGUSD', 'XAUUSD', 'AUDCAD', 'AUDCHF', 'AUDJPY', 'AUDNZD', 'AUDSGD', 'CADCHF', 'CADHKD', 'CADJPY', 'CHFJPY', 'CHFSGD', 'EURAUD', 'EURCAD', 'EURCHF', 'EURCZK', 'EURDKK', 'EURGBP', 'EURHKD', 'EURHUF', 'EURJPY', 'EURNOK', 'EURNZD', 'EURPLN', 'EURRUB', 'EURSEK', 'EURSGD', 'EURTRY', 'GBPAUD', 'GBPCAD', 'GBPCHF', 'GBPJPY', 'GBPNZD', 'HKDJPY', 'NZDCAD', 'NZDCHF', 'NZDJPY', 'SGDJPY', 'TRYJPY', 'USDCNH', 'USDCZK', 'USDDKK', 'USDHUF', 'USDILS', 'USDMXN', 'USDNOK', 'USDPLN', 'USDRON', 'USDRUB', 'USDSEK', 'USDSGD', 'USDTHB', 'USDTRY', 'USDZAR', 'ZARJPY']
    instruments = list(set(instruments))
    for ticker in instruments:
        ticker= ticker.lower()
        try:
            G = graphinatePrime(frame, ticker, 'max', 0)
            chk = chkSell(ticker, signals, G, pos_threshold, neg_threshold, threshold)
            print(ticker+" signal evaluates to "+str(chk))
            if chk == True:
                retList.append(ticker)
        except:
            print(ticker+" failed")
    return retList

def getCandles(instrument):
    print("Retreiving candles for "+str(instrument)+ ", my lord...")
    process = subprocess.Popen(["v20-instrument-candles", instrument], stdout=subprocess.PIPE)
    out, err = process.communicate()
    string = ""
    with open("candles.txt", "r") as text_file:
        string = text_file.read()
    lines = string.split("\n")
    candles = []
    counter = 0
    newdict = dict()
    for i in range(0, len(lines)-1):
        counter = i % 7
        line = lines[i]
        if counter == 0 and i == 0:
            newdict = dict()
            splt = line.split(": ")
            newdict[splt[0]] = splt[1]
        elif counter == 0 and i > 0:
            newdict = dict()
            splt = line.split("time: ")
            newdict["time"] = splt[1]
        if counter > 0:
            splt = line.split(":")
            newdict[splt[0]] = splt[1]
        if counter == 6:
            candles.append(newdict)
    frameron = pd.DataFrame(candles)
    frameron = frameron.rename(columns={'  c':'close', '  h':'high', '  l':'low', '  o':'open'})
    frameron['high'] = frameron['high'].apply(float)
    frameron['close'] = frameron['close'].apply(float)
    frameron['low'] = frameron['low'].apply(float)
    frameron['open'] = frameron['open'].apply(float)
    frameron['deriv'] = frameron['high'].diff()
    return frameron


def MakeSignal(instrument):
    df = getCandles(instrument)
    return df['deriv'].sum()


def getSignals():
    instruments = ['XAUUSD', 'XAGUSD', 'GBPUSD', 'NZDUSD', 'USDCAD', 'USDJPY', 'USDCHF', 'USDCAD', 'GBPUSD', 'EURUSD', 'AUDUSD', 'XAGUSD', 'XAUUSD', 'AUDCAD', 'AUDCHF', 'AUDJPY', 'AUDNZD', 'AUDSGD', 'CADCHF', 'CADHKD', 'CADJPY', 'CHFJPY', 'CHFSGD', 'EURAUD', 'EURCAD', 'EURCHF', 'EURCZK', 'EURDKK', 'EURGBP', 'EURHKD', 'EURHUF', 'EURJPY', 'EURNOK', 'EURNZD', 'EURPLN', 'EURRUB', 'EURSEK', 'EURSGD', 'EURTRY', 'GBPAUD', 'GBPCAD', 'GBPCHF', 'GBPJPY', 'GBPNZD', 'HKDJPY', 'NZDCAD', 'NZDCHF', 'NZDJPY', 'SGDJPY', 'TRYJPY', 'USDCNH', 'USDCZK', 'USDDKK', 'USDHUF', 'USDILS', 'USDMXN', 'USDNOK', 'USDPLN', 'USDRON', 'USDRUB', 'USDSEK', 'USDSGD', 'USDTHB', 'USDTRY', 'USDZAR', 'ZARJPY']
    instruments = list(set(instruments))
    signals = dict()
    for instrument in instruments:
        inst = instrument.lower()
        sig = MakeSignal(instrument)
        if sig > 0:
            sigstr = "up"
        elif sig < 0:
            sigstr = "down"
        else:
            sigstr = "stagnant"
        signals[inst] = sigstr
    return signals


def doBuys(frame, pos_threshold, neg_threshold, threshold):
    BuyList = MakeBuys(frame, pos_threshold, neg_threshold, threshold)
    if len(BuyList) > 0:
        for buy in BuyList:
            ticker  = buy[:3].upper()+"_"+buy[3:].upper()
            print("Ordering: "+str(ticker)+", master...")
            process = subprocess.Popen(["v20-order-market", ticker, "1"], stdout=subprocess.PIPE)
            out, err = process.communicate()
    else:
        print("Not enough movement for signals at the moment")
def doSells(frame, pos_threshold, neg_threshold, threshold):
    SellList = MakeSells(frame, pos_threshold, neg_threshold, threshold)
    if len(SellList) > 0:
        for sell in SellList:
            ticker  = sell[:3].upper()+"_"+sell[3:].upper()
            print("Selling: "+str(ticker)+", from our cache master...")
            process = subprocess.Popen(["v20-order-market", ticker, "-1"], stdout=subprocess.PIPE)
            out, err = process.communicate()
    else:
        print("Not enough movement for signals at the moment, my lord")

def doTrades(frame, pos_threshold, neg_threshold, threshold):
    instruments = ['XAUUSD', 'XAGUSD', 'GBPUSD', 'NZDUSD', 'USDCAD', 'USDJPY', 'USDCHF', 'USDCAD', 'GBPUSD', 'EURUSD', 'AUDUSD', 'XAGUSD', 'XAUUSD', 'AUDCAD', 'AUDCHF', 'AUDJPY', 'AUDNZD', 'AUDSGD', 'CADCHF', 'CADHKD', 'CADJPY', 'CHFJPY', 'CHFSGD', 'EURAUD', 'EURCAD', 'EURCHF', 'EURCZK', 'EURDKK', 'EURGBP', 'EURHKD', 'EURHUF', 'EURJPY', 'EURNOK', 'EURNZD', 'EURPLN', 'EURRUB', 'EURSEK', 'EURSGD', 'EURTRY', 'GBPAUD', 'GBPCAD', 'GBPCHF', 'GBPJPY', 'GBPNZD', 'HKDJPY', 'NZDCAD', 'NZDCHF', 'NZDJPY', 'SGDJPY', 'TRYJPY', 'USDCNH', 'USDCZK', 'USDDKK', 'USDHUF', 'USDILS', 'USDMXN', 'USDNOK', 'USDPLN', 'USDRON', 'USDRUB', 'USDSEK', 'USDSGD', 'USDTHB', 'USDTRY', 'USDZAR', 'ZARJPY']
    instruments = list(set(instruments))
    siggies = getSignals()
    BuyList = LiteBuys(frame, siggies, pos_threshold, neg_threshold, threshold)
    SellList = LiteSells(frame, siggies, pos_threshold, neg_threshold, threshold)
    if len(BuyList) > 0:
        for buy in BuyList:
            ticker  = buy[:3].upper()+"_"+buy[3:].upper()
            print("Ordering: "+str(ticker)+", master...")
            process = subprocess.Popen(["v20-order-market", ticker, "1"], stdout=subprocess.PIPE)
            out, err = process.communicate()
            lines = out.splitlines()
            for line in lines:
                print(line.decode("utf-8"))
    else:
        print("No Buys Found Right Now")
    if len(SellList) > 0:
        for sell in SellList:
            ticker  = sell[:3].upper()+"_"+sell[3:].upper()
            print("Selling: "+str(ticker)+", master...")
            process = subprocess.Popen(["v20-order-market", ticker, "1"], stdout=subprocess.PIPE)
            out, err = process.communicate()
            lines = out.splitlines()
            for line in lines:
                print(line.decode("utf-8"))
    else:
        print("No Buys Found Right Now")

def showmenu():
    on = True
    print("Select network to map:")
    print("1. Averages")
    print("2. Highs")
    print("3. Lows")
    choice = input("Choose one of the numbers above: ")
    frame = treated_avg
    if (choice == "1"):
        frame = treated_avg
    elif (choice == "2"):
        frame = treated_high
    elif (choice == "3"):
        frame = treated_low 
    threshold = input("Select confidence threshold: ")
    try:
        threshold = float(threshold)
        if threshold > 1 or threshold < -1:
            threshold = 0
    except:
        print("user input error")
        threshold = 0

    pos_threshold = input("Select positive bound of for signal indicator: ")
    try:
        pos_threshold = float(pos_threshold)
        if pos_threshold > 1 or pos_threshold < -1:
            pos_threshold = 0
    except:
        pos_threshold = 0
        print("user input error")
        
    neg_threshold = input("Select negative bound of for signal indicator: ")
    try:
        neg_threshold = float(neg_threshold)
        if neg_threshold > 1 or neg_threshold < -1:
            neg_threshold = 0
    except:
        neg_threshold = 0
        print("user input error")
    while(on):
        doTrades(frame, pos_threshold, neg_threshold, threshold)
        process = subprocess.Popen(["v20-account-summary"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        lines = stdout.splitlines()
        for line in lines:
            print(line.decode("utf-8"))






def main():
    try:
        showmenu()
    except KeyboardInterrupt:
        print("Exiting Trade My Master")
        try:
            process.kill()
        except:
            pass
        pass

main()

