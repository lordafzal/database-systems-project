#!/bin/bash

tickers=('AUDCAD' 'AUDCHF' 'AUDJPY' 'AUDNZD' 'AUDSGD' 'CADCHF' 'CADHKD' 'CADJPY' 'CHFJPY' 'CHFSGD' 'EURAUD' 'EURCAD' 'EURCHF' 'EURCZK' 'EURDKK' 'EURGBP' 'EURHKD' 'EURHUF' 'EURJPY' 'EURNOK' 'EURNZD' 'EURPLN' 'EURRUB' 'EURSEK' 'EURSGD' 'EURTRY' 'GBPAUD' 'GBPCAD' 'GBPCHF' 'GBPJPY' 'GBPNZD' 'HKDJPY' 'NZDCAD' 'NZDCHF' 'NZDJPY' 'SGDJPY' 'TRYJPY' 'USDCNH' 'USDCZK' 'USDDKK' 'USDHDK' 'USDHUF' 'USDILS' 'USDMXN' 'USDNOK' 'USDPLN' 'USDRON' 'USDRUB' 'USDSEK' 'USDSGD' 'USDTHB' 'USDTRY' 'USDZAR' 'ZARJPY')

counter=1
day=1  
while [ $counter -le ${#tickers[@]} ]
do
while [ $day -le 31 ]
do
if [ $day -le 10 ]
then
	dayt="0$day"
else
	dayt=$day
fi

echo 'Accessing: ' ${tickers[$counter]} $dayt

duka ${tickers[$counter]} -d 2018-9-$dayt
psql -c "\COPY "${tickers[$counter]}"(time, "${tickers[$counter]}"_Bid, "${tickers[$counter]}"_Ask, "${tickers[$counter]}"_volBid, "${tickers[$counter]}"_volAsk)  FROM '/root/Data/"${tickers[$counter]}"-2018_09_"$dayt"-2018_09_"$dayt".csv' DELIMITER ',' CSV HEADER;"
((day++))
done
((counter++))
done
echo All done

