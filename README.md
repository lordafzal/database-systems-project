# Eliyah Afzal & James Hicks
# ITWS Database Systems Fall 2018

#### Only part you need to read:
```
python3 Application.py
```
user: explorer
pass: s0j0urn3r
date>= '2018-01-01'

at times there has arisen a dependency problem that is solvable using
```
python3 -m pip install --upgrade networkx
```

# Data Information

## Data Source 1 - Foreign Exchange Market
https://www.dukascopy.com/swiss/english/marketwatch/historical/

Who: Dukascopy is a Swiss banking group that provides historical price data for a variety of financial instruments (Foreign Exchange Market).
What: Prices of various currencies paired by date/time.
When: Accessed Fall 2018, data loaded depends when the database is built. The existing database contains data from January 1st 2018 to present.
Why: Preservation aside, currencies represent strength of an economy, and analysis of the exchange rates can illustrate relationships and result in knowledge or information discovery.

Data Columns are self describing



Terms of use: https://www.dukascopy.com/plugins/cont.php?ref_id=198934
Summary: The data cannot be used commercially, for illegal purposes, or in any manner that could compete with Dukascopy.
The data is owned by Dukascopy.


## Data Source 2 - Stock
Yahoo! Finance

Who: Yahoo Finance is a media property that is a part of Yahoo!'s network. Their media focuses on stock quotes and related news.
What: Prices of various stocks paired by date/time.
When: Accessed Fall 2018, data loaded depends when the database is built. The existing database contains data from January 1st 2018 to present.
Why: Preservation aside, stocks make up a market, and markets can be analyzed for insights and knowledge.

Data Columns are self describing

Terms of use: https://developer.yahoo.com/terms/
Summary: The data cannot be used for illegal or malicious purposes. Yahoo! is not responsible for any losses due to actions that derive from the data.

## Data Source 3 - Cryptocurrency
https://coinmarketcap.com/

Who: CoinMarketCap is a platform created to track the capitalization of different cryptocurrencies, the amount of trades that use them and the current price converted into fiat currencies.
What: Various cryptocurrencies and various metrics.
When: Accessed Fall 2018, data loaded depends when the database is built. The existing database contains data from January 1st 2018 to present.
Why: Preservation aside, cryptocurrencies are a rising market force and should not be ignored.

Data Columns are self describing

Terms of use: https://coinmarketcap.com/terms/
Summary: The data cannot be used for illegal or malicious purposes.



## Data Source 1, 2, AND 3

Why: The merged data can be used to investigate the relationships between companies and currencies. Machine learning models could be used to predict the future of a stock or currency.



# Database or File Information

## Creating database
NOTE: This section is not required if the user intends to SSH into the existing database.
Run Schema to create the tables and attributes require for the data.
Set DailyDownloader and CreateAggregateTable as scheduled tasks to update the database with new data.
The first execution will populate the database with data from the prior day.

If the user wants to obtain data prior to the day before execution, run autoDL_data to obtain data from the past month.

## Building Application
Run the following line in terminal to install the requirements for the application:
pip install -r requirements.txt
Run the following line in terminal to execute the application
python Application.py

NOTE: the line above may need to be "python3 Application.py" depending on the users default python environment.


## Using the Application

Application.py can be used to SSH into the database. The application loads in the data, merges it on date, converts it to a graphical format, then outputs the desired visualizations.
The data is stored in a python object, thus the


The application has a few major prompts.

The application will ask the user "Which Prices shall we examine?" where the user can enter the number they desire to examine as prompted. The application will then load another menu based on that selection. For example, if the user selects 1 for stock indexes, the stock menu will open.

In further menus, The application asks the user "Which metrics should we look at?" and the user must enter a number again.
Once a metric is determined, the user is prompted for the type of graph they would like to see in the same manner.

This process is identical for the other options, such as Foreign Currencies for example.

We have added support for inputting your own SQL statements, however remember that all the relevant tables are in the schema Prices so queries should be Prices.* to actually find what your looking for.


## Logging (Grad requirement)
Changes to the database are scheduled and logged in a table named changeLog. Since the system is designed to update daily the table contains the two columns, the day added and the day the changes were made.

## Users
On the one hand the application does not provide any avenue for the user to input SQL strings into the database only to select menu options which handle the data before displaying it. Beyond this relatively straightforward defense against SQL injection the application logs itself in as a user who does not have privileges to change data but only to view it. The DailyDownloader and CreateAggregateTable.sh should be scheduled (in our setup cron job) by the Admin user which in our case is "root" but it can be "postgres" we didn't include the creation of this user in the schema bash because it would have to be the user running the schema bash itself.

Since we added a login process, where the application requires a valid username and password to the postgres before it starts you will need at least one working login to use the application we have at least two already made but we also added an option for you to make more. 

### Existing Users
user - explorer
pass - s0j0urn3r

user - bilbo
pass - baggins

### Included Files
Schema - bash that makes all the tables, the larger tables at the bottom may throw errors if the database if unfilled but it doesn't matter because they have their own scripts as well
Application.py - this is the python application which the user is meant to run, after installing dependencies this script should actually be all the user needs to explore that database.
##### Scheduled Tasks
DailyDownloader - script to be added to crontab so that the database keeps itself up to database
CreateLargeIndexTable - this helps create a table which sets the stage for the aggregate table
CreateAggregateTable - this table concatenates aggregate data from the other tabels so it can be used to make inferences we only run it twice a week but it would actually be fine to run it everyday right after the DailyDownloader.
##### Misc
autoDL_Data - a reworked daily downloader utility for downloading whole months at a time, useful for backfilling Database
While these scripts facilitate making your own database they are actually unnecessary because the application reaches out to our remote database which has already been loaded with the prices data and for which the scheduled tasks have been running for many days besides the addition of back data.
IndexDownloader, IndexLoader  - these are both helper scripts for the DailyDownloader which as expected download index data and load it into the database. 
requirements.txt - just in case you don't have all the packages already in your python environment you can pip install from the requirements.txt to catch up on all the dependencies.