#!/bin/bash
counter=0
tickers=("GSPC" "DJI" "IXIC" "NYA" "XAX" "BUK100P" "RUT" "VIX" "GDAXI" "FCHI" "STOXX50E" "N100" "BFX" "MOEX.ME" "HSI" "00001.SS" "AXJO" "AORD" "BSESN" "JKSE" "NZ50" "KS11" "TWII" "GSPTSE" "BVSP" "MXX" "IPSA" "MERV" "TA125.TA" "CASE30" "JN0U.JO")


psql -c "set datestyle to MDY, SQL;"
while [ $counter -le $(expr ${#tickers[@]} - 1) ]
do
psql "Prices" -c "CREATE TABLE "${tickers[$counter]}"(date DATE, "${tickers[$counter]}"_Open NUMERIC, "${tickers[$counter]}"_High NUMERIC, "${tickers[$counter]}"_Low NUMERIC, "${tickers[$counter]}"_Close NUMERIC, "${tickers[$counter]}"_AdjClose NUMERIC, "${tickers[$counter]}"_Volume NUMERIC);"
((counter++))
done
echo All done
