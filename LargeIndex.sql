
 CREATE TABLE Prices.all_indeces(date DATE);

 INSERT INTO all_indexes(date) SELECT date from Prices.vix;

ALTER TABLE Prices.all_indeces ADD COLUMN 
	vix_Open NUMERIC,
	ADD COLUMN vix_High NUMERIC,
	ADD COLUMN vix_Low NUMERIC, 
	ADD COLUMN vix_Close NUMERIC, 
	ADD COLUMN vix_AdjClose NUMERIC, 
	ADD COLUMN vix_Volume NUMERIC;

UPDATE Prices.all_indeces SET vix_Open = 
	(SELECT vix_Open FROM Prices.vix WHERE Prices.all_indeces.date = Prices.vix.date)
	WHERE EXISTS (SELECT vix_Open FROM Prices.vix WHERE Prices.all_indeces.date = Prices.vix.date);


















-- SELECT * FROM Prices.gspc NATURAL JOIN Prices.DJI
--  NATURAL JOIN Prices.IXIC
--  NATURAL JOIN Prices.NYA
--  NATURAL JOIN Prices.XAX
--  NATURAL JOIN Prices.BUK100P
--  NATURAL JOIN Prices.RUT
--  NATURAL JOIN Prices.VIX
--  NATURAL JOIN Prices.GDAXI
--  NATURAL JOIN Prices.FCHI
--  NATURAL JOIN Prices.STOXX50E
--  NATURAL JOIN Prices.N100
--  NATURAL JOIN Prices.BFX
--  NATURAL JOIN Prices.HSI
--  NATURAL JOIN Prices.SS
--  NATURAL JOIN Prices.AXJO
--  NATURAL JOIN Prices.AORD
--  NATURAL JOIN Prices.BSESN
--  NATURAL JOIN Prices.JKSE
--  NATURAL JOIN Prices.NZ50
--  NATURAL JOIN Prices.KS11
--  NATURAL JOIN Prices.TWII
--  NATURAL JOIN Prices.GSPTSE
--  NATURAL JOIN Prices.BVSP
--  NATURAL JOIN Prices.MXX
--  NATURAL JOIN Prices.IPSA
--  NATURAL JOIN Prices.MERV
--  NATURAL JOIN Prices.TA125TA
--  NATURAL JOIN Prices.JN0UJO
--  ORDER BY "date" DESC;
