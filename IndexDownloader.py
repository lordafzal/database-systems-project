print 'Initiating Download Sequence'

from yahoo_historical import Fetcher
from datetime import datetime
today = datetime.now()

tickers = ["^GSPC", "^DJI", "^IXIC", "^NYA", "^XAX", "^BUK100P", "^RUT", "^VIX", "^GDAXI", "^FCHI", "^STOXX50E", "^N100", "^BFX", "IMOEX.ME", "^HSI", "000001.SS", "^AXJO", "^AORD", "^BSESN", "^JKSE", "^NZ50", "^KS11", "^TWII", "^GSPTSE", "^BVSP", "^MXX", "^IPSA", "^MERV", "^TA125.TA", "^JN0U.JO"]

print 'Processing...'
for ticker in tickers:
	print "Downloading: " + ticker
	data = Fetcher(ticker, [2016,1,1], [today.year,today.month,today.day-1])
	filename = ticker.replace('^', '').replace('.', '').replace('000001', '')
	data.getHistorical().to_csv("/root/Data/Indexes/" + filename + ".csv", index=False)
	print "Finally Accessed"
